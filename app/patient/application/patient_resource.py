from flask import request, jsonify, Blueprint
from flask import current_app as app
import json

from patient.domain.patient import Patient

patient_bp = Blueprint('patient', __name__)

@patient_bp.route('/patients', methods=['POST'])
def create_patient():
    try:
        patient_data = request.get_json()
        patient_service = app.config['patient_service']
        created_patient = patient_service.create_patient(Patient(**patient_data))

        return jsonify(created_patient.to_dict()), 201
    except Exception as e:
        return jsonify({'error': str(e)}), 400
    
@patient_bp.route('/patients', methods=['GET'])
def get_all_patients():
    try:
        patient_service = app.config['patient_service']
        found_patients = patient_service.get_all_patients()
        patients_json = json.dumps([patient.__dict__ for patient in found_patients])

        return jsonify(patients_json), 201
    except Exception as e:
        return jsonify({'error': str(e)}), 400
    
@patient_bp.route('/patients/<int:patient_id>', methods=['GET'])
def get_patient(patient_id):
    try:
        patient_service = app.config['patient_service']
        patient = patient_service.get_patient(patient_id)

        if not patient:
            return jsonify({'error': 'Patient not found'}), 404

        return jsonify(patient.to_dict())
    except Exception as e:
        return jsonify({'error': str(e)}), 400
    
@patient_bp.route('/patients/<int:patient_id>', methods=['PUT'])
def update_patient(patient_id):
    try:
        patient_data = request.get_json()
        patient_service = app.config['patient_service']
        updated_patient = patient_service.update_patient(patient_id , Patient(**patient_data))

        return jsonify(updated_patient.to_dict())
    except Exception as e:
        return jsonify({'error': str(e)}), 400

@patient_bp.route('/patients/<int:patient_id>', methods=['DELETE'])
def delete_patient(patient_id):
    try:
        patient_service = app.config['patient_service']
        patient = patient_service.delete_patient(patient_id)

        if not patient:
            return jsonify({'error': 'Patient not found'}), 404

        return jsonify(patient.to_dict())
    except Exception as e:
        return jsonify({'error': str(e)}), 400